/*
 * Jogo da velha
 * baseado no código fonte disponível em http://goo.gl/o13trS
 */
package jogodavelha;

import java.net.Socket;

public class Game {
    private int id;
    private Board board;
    private int round = 1, turn = 1;
    private Player player1;
    private Player player2;

    public Game() {
        board = new Board();
        initPlayers();

//        while (play()) ;
    }

    public Game(int id, String firstPlayerName, Socket firstPlayerSocket, String secondPlayerName, Socket secondPlayerSocket) {
        this.id = id;
        board = new Board();
        initPlayers();
        player1.setName(firstPlayerName);
        player1.setSocket(firstPlayerSocket);
        player2.setName(secondPlayerName);
        player2.setSocket(secondPlayerSocket);
//        while (play()) ;
    }

    public void initPlayers() {
        this.player1 = new Player(1);
        this.player2 = new Player(2);
    }

    public Player getPlayer1() {
        return player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public boolean play() {
        if (win() == 0) {
            System.out.println("----------------------");
            System.out.println("\nRodada " + round);
            System.out.println("É a vez do jogador " + turn());

            if (turn() == 1)
                player1.play(board);
            else
                player2.play(board);


            if (board.isFull()) {
                System.out.println("Tabuleiro Completo. Jogo empatado");
                return false;
            }
            turn++;
            round++;

            return true;
        } else {
            if (win() == -1)
                System.out.println("Jogador 1 ganhou!");
            else
                System.out.println("Jogador 2 ganhou!");

            return false;
        }
    }

    public Player getCurrentPlayer() {
        return (turn() == 1) ? player1 : player2;
    }

    public void play(int row, int column) {
        if (turn() == 1) {
            player1.play(board, row, column);
        } else {
            player2.play(board, row, column);
        }
        turn++;
    }

    public int turn() {
        if (turn % 2 == 1)
            return 1;
        else
            return 2;
    }

    public int win() {
        if (board.checkRows() == 1)
            return 1;
        if (board.checaColumns() == 1)
            return 1;
        if (board.checkDiagonals() == 1)
            return 1;

        if (board.checkRows() == -1)
            return -1;
        if (board.checaColumns() == -1)
            return -1;
        if (board.checkDiagonals() == -1)
            return -1;

        return 0;
    }

    public Board getBoard() {
        return board;
    }

}
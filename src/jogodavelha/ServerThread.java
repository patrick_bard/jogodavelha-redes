package jogodavelha;

import java.io.*;
import java.net.Socket;

/**
 * Created by 'Patrick on 06/06/2015.
 * IntelliJ IDEA 14.1.1
 */
public class ServerThread extends Thread {
    protected Server server;
    protected Socket socket;
    protected String playerName;
    protected BufferedReader inFromClient;
    protected BufferedWriter outToClient;

    public ServerThread(Server server, Socket clientSocket) throws IOException {
        this.server = server;
        this.socket = clientSocket;
        this.inFromClient = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        this.outToClient = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
    }

    public void run() {
        try {
            String line = inFromClient.readLine();
            if(line != null) {
                String[] clientCommand = line.split(" ");
                String commandType = clientCommand[0];
                if (commandType.equalsIgnoreCase("JOIN")) {
                    playerName = clientCommand[1];
                    System.out.println(playerName + " joined");
                    Server.players.add(this);
                }
            }
        } catch (IOException ignored) {}
    }
}

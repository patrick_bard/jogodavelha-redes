package jogodavelha;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;

/**
 * Created by 'Patrick on 13/06/2015.
 * IntelliJ IDEA 14.1.1
 */
public class GameServiceThread extends Thread {

    private Game game;

    public GameServiceThread() {}

    public void run(){
        final int gameId = Server.games.size()+1;
        final int lastPlayer = Server.players.size()-1;
        String firstPlayerName = Server.players.get(lastPlayer-1).playerName;
        Socket firstPlayerSocket = Server.players.get(lastPlayer-1).socket;
        String secondPlayerName = Server.players.get(lastPlayer).playerName;
        Socket secondPlayerSocket = Server.players.get(lastPlayer).socket;
        game = new Game(gameId,firstPlayerName,firstPlayerSocket,secondPlayerName,secondPlayerSocket);
        Server.games.add(game);
        try {
            game.getPlayer1().output.write("START " + gameId + " 1 " + secondPlayerName +"\n");
            game.getPlayer2().output.write("START " + gameId + " 2 " + firstPlayerName +"\n");
            game.getPlayer1().output.flush();
            game.getPlayer2().output.flush();

            new Thread(){
                public void run(){
                    String line;
                    String errorMsg = "ERROR " + gameId + " \n";
                    try {
                        BufferedWriter outToPlayer1 = game.getPlayer1().output;
                        BufferedWriter outToPlayer2 = game.getPlayer2().output;

                        BufferedReader inFromPlayer1 = game.getPlayer1().input;
                        while ((line = inFromPlayer1.readLine()) != null) {
                            line += " \n";
                            String[] commandSentence = line.split(" ");
                            String userCommandType = commandSentence[0];
                            if (userCommandType != null) {
                                switch (userCommandType) {
                                    case "PLAY":
                                        System.out.println("GAME "+gameId+" STATUS:");
                                        game.play(Integer.parseInt(commandSentence[2]), Integer.parseInt(commandSentence[3]));
                                        outToPlayer1.write(line);
                                        outToPlayer2.write(line);
                                        outToPlayer1.flush();
                                        outToPlayer2.flush();
                                        break;
                                    case "MSG":
                                        System.out.println(line);
                                        outToPlayer2.write(line);
                                        outToPlayer2.flush();
                                        break;
                                    case "STOP":
                                        System.out.println(line);
                                        outToPlayer1.write(line);
                                        outToPlayer2.write(line);
                                        outToPlayer1.flush();
                                        outToPlayer2.flush();
                                        break;
                                }
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }.start();

            new Thread(){
                public void run(){
                    String line;
                    String errorMsg = "ERROR " + gameId + " \n";
                    try {
                        BufferedWriter outToPlayer1 = game.getPlayer1().output;
                        BufferedWriter outToPlayer2 = game.getPlayer2().output;

                        BufferedReader inFromPlayer2 = game.getPlayer2().input;
                        while ((line = inFromPlayer2.readLine()) != null) {
                            line += " \n";
                            String[] commandSentence = line.split(" ");
                            String userCommandType = commandSentence[0];
                            if (userCommandType != null) {
                                switch (userCommandType) {
                                    case "PLAY":
                                        System.out.println("GAME "+gameId+" STATUS:");
                                        game.play(Integer.parseInt(commandSentence[2]), Integer.parseInt(commandSentence[3]));
                                        outToPlayer1.write(line);
                                        outToPlayer2.write(line);
                                        outToPlayer1.flush();
                                        outToPlayer2.flush();
                                        break;
                                    case "MSG":
                                        System.out.println(line);
                                        outToPlayer1.write(line);
                                        outToPlayer1.flush();
                                        break;
                                    case "QUIT":
                                        System.out.println(line);
                                        outToPlayer1.write("STOP "+gameId+"\n");
                                        outToPlayer2.write("STOP "+gameId+"\n");
                                        outToPlayer1.flush();
                                        outToPlayer2.flush();
                                        break;
                                }
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        } catch (IOException ignored) {
        }
    }
}

package jogodavelha;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 'Patrick on 06/06/2015.
 * IntelliJ IDEA 14.1.1
 */
public class Server {

    public static List<Game> games = new ArrayList<>();
    protected static List<ServerThread> players = new ArrayList<>();

    public static void main(String[] args) throws Exception {
        int serverPort = 8945;
        Server server = new Server();
        ServerSocket welcomeSocket = new ServerSocket(serverPort);
        while (true) {
            Socket connectionSocket = welcomeSocket.accept();
            ServerThread st = new ServerThread(server,connectionSocket);
            st.start();
            st.join();
            if(Server.players.size()>0 && Server.players.size()%2==0){
                new GameServiceThread().start();
            }
        }
    }
}

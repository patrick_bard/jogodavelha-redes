package jogodavelha;

import java.io.*;
import java.net.Socket;

/**
 * Created by 'Patrick on 06/06/2015.
 * IntelliJ IDEA 14.1.1
 */
public class Client {

    private static Game game;
    private static int gameID;
    private static int order;
    private static String opponent;

    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            System.out.println("Usage: java Client <serverIp>");
            System.exit(1);
        }
        String serverIP = args[0];
        int serverPort = 8945;
        final BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
        final Socket clientSocket = new Socket(serverIP, serverPort);

        new Thread(){
            public void run(){
                String line;
                try {
                    BufferedWriter outToServer = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
                    while ((line = inFromUser.readLine()) != null) {
                        String[] commandSentence = line.split(" ");
                        String userCommandType = commandSentence[0];
                        if (userCommandType != null) {
                            switch (userCommandType) {
                                case "/entra":
                                    String nickname = commandSentence[1];
                                    outToServer.write("JOIN " + nickname + '\n');
                                    outToServer.flush();
                                    break;
                                case "/jogar":
                                    if(game.turn() == order){
                                        if (game.getCurrentPlayer().attempt(game.getBoard(),
                                                    Integer.parseInt(commandSentence[1]),Integer.parseInt(commandSentence[2]))){
                                            outToServer.write("PLAY " + gameID  + " " + commandSentence[1] + " " + commandSentence[2]+"\n");
                                            outToServer.flush();
                                        }
                                    }else {
                                        System.out.println("Não é a sua vez, aguarde seu adversário.");
                                    }
                                    break;
                                case "/msg":
                                    outToServer.write("MSG " + gameID + " " + line.substring(5) + "\n");
                                    outToServer.flush();
                                    break;
                                case "/sair":
                                    outToServer.write("QUIT " + gameID + " 0"+"\n");
                                    outToServer.flush();
                                    break;
                            }
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();

        new Thread(){
            public void run(){
                BufferedReader inFromServer;
                try {
                    inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                    String serverResponse;
                    while (true){
                        serverResponse = inFromServer.readLine();
                        if(serverResponse != null) {
                            String[] serverCommand = serverResponse.split(" ");
                            String serverCommandType = serverCommand[0];
                            switch (serverCommandType) {
                                case "START":
                                    game = new Game();
                                    gameID = Integer.parseInt(serverCommand[1]);
                                    order = Integer.parseInt(serverCommand[2]);
                                    opponent = serverCommand[3];
                                    System.out.println("Um novo jogo foi iniciado.");
                                    System.out.println("Seu adversario é " + opponent);
                                    if (order == 1) {
                                        System.out.println("Você joga primeiro");
                                    } else {
                                        System.out.println(opponent + " joga primeiro\n");
                                        System.out.println("Esperando o adversário jogar...");
                                    }
                                    break;
                                case "PLAY":
                                    game.play(Integer.parseInt(serverCommand[2]),Integer.parseInt(serverCommand[3]));
                                    String turn = (game.turn() == order)? "É sua vez.":"Aguardando seu adversário";
                                    System.out.println(turn);
                                    break;
                                case "MSG":
                                    System.out.println("<"+opponent+"> "+serverResponse.substring(6));
                                    break;
                                case "STOP":
                                    System.out.println("Jogo finalizado");
                                    System.out.println("Fim.");
                                    System.exit(0);
                                    break;
                            }
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }
}

/*
 * Jogo da velha
 * baseado no código fonte disponível em http://goo.gl/o13trS
 */
package jogodavelha;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.Scanner;

public class Player {
    public Scanner _input_ = new Scanner(System.in);
    protected Socket socket;
    protected BufferedReader input;
    protected BufferedWriter output;
    protected int[] attempt = new int[2];
    protected String name;
    protected int player;


    public Player(int player) {
        this.player = player;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void play(Board board) {
        attempt(board);
        board.setPosition(attempt, player);
    }

    public void play(Board board, int row, int column) {
        attempt(board, row, column);
        board.setPosition(attempt, player);
    }

    public void attempt(Board board) {
        do {
            do {
                System.out.print("Linha: ");
                attempt[0] = _input_.nextInt();
                if (attempt[0] > 3 || attempt[0] < 1)
                    System.out.println("Linha inválida. É 1, 2 ou 3");
            } while (attempt[0] > 3 || attempt[0] < 1);

            do {
                System.out.print("Coluna: ");
                attempt[1] = _input_.nextInt();
                if (attempt[1] > 3 || attempt[1] < 1)
                    System.out.println("Coluna inválida. É 1, 2 ou 3");
            } while (attempt[1] > 3 || attempt[1] < 1);

            attempt[0]--;
            attempt[1]--;

            if (!checkAttempt(attempt, board))
                System.out.println("Esse local já foi marcado. Tente outro.");
        } while (!checkAttempt(attempt, board));
    }

    public boolean attempt(Board board, int row, int column) {
        do {
            do {
                attempt[0] = row;
                if (attempt[0] > 3 || attempt[0] < 1) {
                    System.out.println("Linha inválida. Pode ser apenas 1, 2 ou 3");
                    return false;
                }
            } while (attempt[0] > 3 || attempt[0] < 1);

            do {
                attempt[1] = column;
                if (attempt[1] > 3 || attempt[1] < 1) {
                    System.out.println("Coluna inválida. É 1, 2 ou 3");
                    return false;
                }
            } while (attempt[1] > 3 || attempt[1] < 1);

            attempt[0]--;
            attempt[1]--;
            if (!checkAttempt(attempt, board)) {
                System.out.println("Esse local já foi marcado. Tente outro.");
                return false;
            }
        } while (!checkAttempt(attempt, board));
        return true;
    }

    public boolean checkAttempt(int[] tentativa, Board board) {
        return board.getPosition(tentativa) == 0;

    }

    public void setSocket(Socket socket) {
        this.socket = socket;
        try {
            this.input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.output = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}